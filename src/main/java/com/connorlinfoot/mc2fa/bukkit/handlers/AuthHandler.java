package com.connorlinfoot.mc2fa.bukkit.handlers;

import com.connorlinfoot.mc2fa.bukkit.MC2FA;
import com.connorlinfoot.mc2fa.bukkit.events.PlayerStateChangeEvent;
import com.connorlinfoot.mc2fa.bukkit.storage.FlatStorage;
import com.connorlinfoot.mc2fa.bukkit.utils.ImageRenderer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.map.MapView;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class AuthHandler extends com.connorlinfoot.mc2fa.shared.AuthHandler {
    private MC2FA mc2FA;

    public AuthHandler(MC2FA mc2FA) {
        this.mc2FA = mc2FA;
        this.storageHandler = new FlatStorage(new File(mc2FA.getDataFolder(), "data.yml"));
    }

    public void sendLinkedMessage(Player player, String message, String url) {
        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "tellraw " + player.getName() + " {\"text\":\"\",\"extra\":[{\"text\":\"%%message%%\",\"clickEvent\":{\"action\":\"open_url\",\"value\":\"%%url%%\"}}]}".replaceAll("%%message%%", message).replaceAll("%%url%%", url));
    }

    public void giveQRItem(MC2FA mc2FA, Player player) {
        String url = getQRCodeURL(mc2FA.getConfigHandler().getQrCodeURL(), player.getUniqueId());
        final MessageHandler messageHandler = mc2FA.getMessageHandler();
        if (player.getInventory().firstEmpty() < 0) {
            // Just send a link, inventory is full!
            messageHandler.sendMessage(player, "&aPlease use the QR code given to setup two-factor authentication");
            sendLinkedMessage(player, messageHandler.getMessage("&aPlease click here to open the QR code"), url.replaceAll("128x128", "256x256"));
            messageHandler.sendMessage(player, "&aPlease validate by entering your key: /2fa <key>");
        } else {
            new BukkitRunnable() {
                @Override
                public void run() {
                    MapView view = Bukkit.createMap(player.getWorld());
                    view.getRenderers().forEach(view::removeRenderer);
                    try {
                        ImageRenderer renderer = new ImageRenderer(url);
                        view.addRenderer(renderer);
                        ItemStack mapItem = new ItemStack(Material.MAP, 1, view.getId());
                        ItemMeta mapMeta = mapItem.getItemMeta();
                        mapMeta.setDisplayName(ChatColor.GOLD + "QR Code");
                        mapItem.setItemMeta(mapMeta);

                        if (player.getInventory().firstEmpty() < 0) {
                            // Send message instead!
                            messageHandler.sendMessage(player, "&aPlease use the QR code given to setup two-factor authentication");
                            sendLinkedMessage(player, messageHandler.getMessage("&aPlease click here to open the QR code"), url.replaceAll("128x128", "256x256"));
                            messageHandler.sendMessage(player, "&aPlease validate by entering your key: /2fa <key>");
                        } else {
                            ItemStack itemStack0 = null;
                            if (player.getInventory().firstEmpty() != 0) {
                                itemStack0 = player.getInventory().getItem(0);
                            }
                            player.getInventory().setItem(0, mapItem);
                            if (itemStack0 != null) {
                                player.getInventory().addItem(itemStack0);
                            }
                            player.getInventory().setHeldItemSlot(0);
                            messageHandler.sendMessage(player, "&aPlease use the QR code given to setup two-factor authentication");
                            messageHandler.sendMessage(player, "&aPlease validate by entering your key: /2fa <key>");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        player.sendMessage(ChatColor.RED + "An error occurred! Is the URL correct?");
                    }
                }
            }.runTaskAsynchronously(mc2FA);
        }
    }

    public void playerJoin(UUID uuid) {
        super.playerJoin(uuid);
        Player player = Bukkit.getPlayer(uuid);
        if (player == null || !player.isOnline()) {
            return;
        }

        // Load auth state
        if (getStorageHandler().getKey(uuid) != null) {
            changeState(uuid, AuthState.PENDING_LOGIN);
        } else {
            changeState(uuid, AuthState.DISABLED);
        }

        boolean is2fa = isEnabled(uuid);
        if (is2fa) {
            if (needsToAuthenticate(uuid)) {
                // Require key from 2FA
                // sent it after another messages.
                Bukkit.getScheduler().runTaskLater(mc2FA, () -> {
                    mc2FA.getMessageHandler().sendMessage(player, "&cTwo-factor authentication is enabled on this account");
                    mc2FA.getMessageHandler().sendMessage(player, "&cPlease authenticate using /2fa <key>");
                }, 5L);
            }
        } else {
            if (mc2FA.getConfigHandler().getForced() == ConfigHandler.Forced.TRUE || (player.isOp() && mc2FA.getConfigHandler().getForced() == ConfigHandler.Forced.OP)) {
                // Force 2FA
                mc2FA.getAuthHandler().createKey(player.getUniqueId());
                mc2FA.getAuthHandler().giveQRItem(mc2FA, player);
            }
        }
    }

    public void playerQuit(UUID uuid) {
        super.playerQuit(uuid);
    }

    public void changeState(UUID uuid, AuthState authState) {
        if (authState == getState(uuid))
            return;

        Player player = Bukkit.getPlayer(uuid);
        if (player != null) {
            PlayerStateChangeEvent event = new PlayerStateChangeEvent(player, authState);
            mc2FA.getServer().getPluginManager().callEvent(event);
            if (event.isCancelled())
                return;
            authState = event.getAuthState();
        }

        authStates.put(uuid, authState);
    }

    @Override
    public String getQRCodeURL(String urlTemplate, UUID uuid) {
        String label;
        Player player = Bukkit.getPlayer(uuid);
        if (player != null) {
            label = mc2FA.getConfigHandler().getLabel(player);
        } else {
            label = "";
        }
        return super.getQRCodeURL(urlTemplate, uuid).replaceAll("%%label%%", label);
    }

    public boolean isQRCodeItem(ItemStack itemStack) {
        return itemStack != null && itemStack.getType() == Material.MAP && itemStack.hasItemMeta() && itemStack.getItemMeta().hasDisplayName() && itemStack.getItemMeta().getDisplayName().equals(ChatColor.GOLD + "QR Code");
    }

}
