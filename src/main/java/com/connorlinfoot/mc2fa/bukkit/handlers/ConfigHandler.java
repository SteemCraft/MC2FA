package com.connorlinfoot.mc2fa.bukkit.handlers;

import com.connorlinfoot.mc2fa.bukkit.MC2FA;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class ConfigHandler {
    private String qrCodeURL = "https://www.google.com/chart?chs=128x128&cht=qr&chl=otpauth://totp/%%label%%?secret=%%key%%";
    private String label = "%%name%%:SteemCraft";
    private boolean debug = false;
    private boolean commandsDisabled = true;
    private Forced forced = Forced.FALSE;
    private List<String> whitelistedCommands = new ArrayList<>();
    private List<String> blacklistedCommands = new ArrayList<>();

    public enum Forced {
        TRUE, FALSE, OP
    }

    public ConfigHandler(MC2FA mc2FA) {
        FileConfiguration config = mc2FA.getConfig();

        if (config.isSet("Debug"))
            debug = config.getBoolean("Debug");

        if (config.isSet("Disable Commands"))
            commandsDisabled = config.getBoolean("Disable Commands");

        if (config.isSet("Whitelisted Commands"))
            whitelistedCommands = config.getStringList("Whitelisted Commands");
        whitelistedCommands.add("2fa");
        whitelistedCommands.add("twofactorauth");
        whitelistedCommands.add("twofactorauthentication");
        whitelistedCommands.add("twofa");
        whitelistedCommands.add("tfa");

        if (config.isSet("Blacklisted Commands"))
            blacklistedCommands = config.getStringList("Blacklisted Commands");

        if (config.isSet("Forced")) {
            try {
                forced = Forced.valueOf(config.getString("Forced").toUpperCase());
            } catch (Exception ignored) {
            }
        }

        if (config.isSet("QR Code URL")) {
            qrCodeURL = config.getString("QR Code URL");
        }

        if (config.isSet("OTP Label")) {
            label = config.getString("OTP Label");
        }
    }

    public boolean isDebug() {
        return debug;
    }

    public boolean isCommandsDisabled() {
        return commandsDisabled;
    }

    public List<String> getWhitelistedCommands() {
        return whitelistedCommands;
    }

    public List<String> getBlacklistedCommands() {
        return blacklistedCommands;
    }

    public Forced getForced() {
        return forced;
    }

    public String getQrCodeURL() {
        return qrCodeURL;
    }

    public String getLabel(Player player) {
        if (player == null) {
            return "";
        }
        return label.replaceAll("%%name%%", player.getName());
    }

}
